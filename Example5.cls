public class Example5 {
    public List<SObject> ListAllDuplicate = new List<SObject>();
    public static void func() {
        try {
            Example5 test = new Example5();
            test.ListAllDuplicate.addAll(collectDuplicateField('Contact', 'Name'));
            test.ListAllDuplicate.addAll(collectDuplicateField('Lead', 'Name'));
            delete test.ListAllDuplicate;
        }catch (Exception e) {
            system.debug(e.getMessage());
        }
    }
    public static List<SObject> collectDuplicateField(String apiNameObj, String objFieldDuplicate) {
        List<SObject> allDuplicate = new List<SObject>();
        List<SObject> ListObjects = Database.query('SELECT ' + objFieldDuplicate + ' FROM ' + apiNameObj );
        Set <String> uniqueValue = new Set<String>();
        for (SObject itemSObject : ListObjects) {
                if (uniqueValue.contains(String.valueOf(itemSObject.get(objFieldDuplicate)))) {
                    allDuplicate.add(itemSObject);
                } else {
                    uniqueValue.add(String.valueOf(itemSObject.get(objFieldDuplicate)));
                }
            }
        if (!allDuplicate.isEmpty()) {
            return allDuplicate;
        }
        return null;
    }
}
